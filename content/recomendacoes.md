---
title: "Recomendações"
date: 2021-10-10T22:54:43-03:00
draft: false
---

Se você leu algumas de minhas resenhas, deve ter percebido que eu sou obcecado com grandes séries de fantasia e ficção. Quanto mais épico, quanto mais complexo o mundo, sua política e sua mágica, melhor. Sendo assim, eu queria recomendar aqui algumas das minhas sagas favoritas, e quais são melhores para se começar no incrível mundo da fantasia.

Eu costumo dividir as séries de ficção que eu leio em duas categorias. A primeira consiste nos livros em que a magia e a ficção são construidas em cima do mundo real, seja pela adição de seres e realidades no mundo presente, seja mostrando um mundo no futuro. Algumas das séries que eu mais gosto são:

- Harry Potter
- Os Instrumentos Mortais
- Jogos Vorazes

A outra categoria consiste nos livros que criam um universo completamente diferente do nosso, embora inspirações e metáforas possam ser percebidas. Normalmente esses livros são um pouco mais complexos de ler, pois os autores amam nos jogar no meio de um contexto com poucas explicações no começo. Atualmente, esses são os tipos de série que eu tenho gostado mais. Os autores conseguem controlar todos os aspectos do universo, desde a política até a ecologia, podendo explorar cenários e histórias muito diversas. Alguns favoritos meus são:

- *The Stormlight Archives*
- *A Song of Ice and Fire*
- *The Kingkiller Chronicles*
- *Mistborn*
