---
title: "Dune"
date: 2021-10-10T22:55:24-03:00
draft: false
ano_publicacao: 1965
autor: Frank Herbert
avaliacao: 5
capa: /images/dune.jpg
---

Duna conta a história do jovem Paul Atreides, que sai de seu planeta natal quando sua família é designada pelo imperador para governar o planeta de Arrakis (também conhecido como Duna). Arrakis é um planeta hostil, com grandes tempestades de areia e pouquíssima água, mas o maior perigo é político: os Harkonnens, família inimiga dos Atreides

O estilo da narrativa é bem interessante pelo fato de que o protagonista tem visões do futuro regularmente. Passado, presente e possíveis futuros se misturam na trajetória de Paul em Duna. A construção do mundo também é marcante, com política, ecologia, religião e tecnologia muito bem desenvolvidas, e honestamente diferentes de outras séries que eu já li. É uma obra de proporções épicas, com grandes batalhas e conflitos.

Sendo um dos grandes clássicos da ficção científica, é muito interessante identificar os paralelos entre Duna e outros livros e filmes, tanto em temáticas específicas quanto na estrutura geral da sua construção de mundo.

Esse é o primeiro livro de uma série de seis. Não li ainda os outros, mas certamente a leitura desse primeiro já vale a pena (e dá um bom fechamento na história, se você não quiser ler os demais).
