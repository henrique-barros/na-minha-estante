---
title: "The Name of the Wind"
date: 2021-10-10T22:55:24-03:00
draft: false
ano_publicacao: 2007
autor: Patrick Rothfuss
avaliacao: 5
capa: /images/thenameofthewind.jpg
---

*The Name of the Wind* (O Nome do Vento, na tradução em português) é o primeiro livro na trilogia da "Crônica do Matador do Rei" e facilmente o meu livro favorito de todos os tempos. O livro é narrado pelo próprio protagonista, Kvothe, enquanto ele conta sua história de vida para o Cronista. Kvothe inicia sua trajetória como um jovem pertencente à trupe *Edema Ruh*, cujos integrantes são massacrados por um grupo misterioso chamado *Chandrian*. O rapaz então busca entender o ocorrido enquanto acompanhamos sua vida de órfão até a Universidade, onde estuda para se tornar um arcanista.

Algo que chama atenção na obra é a incrível construção de mundo feita pelo autor. A descrição da política, dos povos e das peculiaridades do mundo de Temerant tornam a leitura extremamente imersiva. O sistema de mágica é muito bem desenvolvido, com um conjunto de regras bem definidas. A jornada do protagonista pela Universidade nos permite explorar até mesmo os aspectos mais técnicos da mágica, mas sem perder de vista o sobrenatural que a torna interessante em primeiro lugar.

Todos esses fatores já fariam um ótimo livro de fantasia, para mim. Mas o que destaca O Nome do Vento acima de outros livros são seus personagens, e a narrativa íntima construida ao redor deles. Ao longo da leitura, primariamente narrado em primeira pessoa, sentimos na pele as dificuldades emocionais e até financeiras de Kvothe. As relações que ele desenvolve com as pessoas que encontra são muito reais, bem como suas dores. E a relação que é estabelecida entre o protagonista e a sua música ao longo de todas as idas e vindas tornam a experiência realmente mágica.