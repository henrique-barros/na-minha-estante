---
title: "The Way of Kings"
date: 2021-10-10T22:55:07-03:00
draft: false
ano_publicacao: 2010
autor: Brandon Sanderson
avaliacao: 5
capa: /images/thewayofkings.jpg
---

*The Way of Kings* é o primeiro livro da série *The Stormlight Archives*. A história se passa no mundo de Roshar, em que tempestades violentas percorrem o globo regularmente, e acompanha principalmente o ponto de vista de três personagens: Kaladin, filho de um cirurgião de uma pequena vila que sai de casa para se juntar ao exército; Shallan, uma menina muito inteligente de uma família nobre buscando salvá-la da falência; e Dalinar, irmão de um rei assassinado e parte de uma guerra para vingar sua morte.

O mundo criado por Sanderson é extremamente rico: as tempestades moldam toda a ecologia, a arquitetura e a magia. A história também explora os acontecimentos do passado, com Desolações que ocorrem periodicamente.

Os arcos de desenvolvimento dos personagens é muito bem construido. Sentimos os conflitos internos, os erros e o amadurecimento dos personagens, cada um imerso em um contexto muito diferente. O livro vale super a pena, mas cuidado: dos 10 livros planejados para a série, só 4 foram lançados até agora...
