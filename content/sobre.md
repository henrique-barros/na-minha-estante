---
title: "Sobre o Autor"
date: 2021-10-10T22:52:15-03:00
draft: false
---

Henrique Barros Oliveira, autor do Na Minha Estante, é um ávido leitor desde a infância. Nascido em Salvador, Bahia, ele logo se apaixonou com o mundo da fantasia e da ficção científica com sagas como "Harry Potter", "Percy Jackson e os Olimpianos" e "As Crônicas de Nárnia".

Atualmente, Henrique mora ~~morava~~ em São Paulo, pois cursa Engenharia Mecatrônica na Poli-USP. Ele faz parte do grupo de pessoas que esperam eternamente os últimos livros de "As Crônicas de Gelo e Fogo", de George R. R. Martin e de "A Crônica do Matador do Rei", de Patrick Rothfuss.
